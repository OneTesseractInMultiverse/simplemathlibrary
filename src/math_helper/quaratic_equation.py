# -----------------------------------------------------------------------------
# FUNCTIONAL PROGRAMMING EXAMPLES
# -----------------------------------------------------------------------------

# Lets convert the quadratic equation into a set of functions.

# The following code finds two solutions for a quadratic equation using _axˆ2+bx+c_
# a: float = 0
# b: float = 0
# c: float = 0
#
# discriminant: float = (b ** 2) - 4 * a * c
#
# x1 = ((-1 * b) - (discriminant) ** (1 / 2)) / (2 * a)
# x2 = ((-1 * b) - (discriminant) ** (1 / 2)) / (2 * a)
#
# print("solution for x1: {}".format(x1))
# print("solution for x2: {}".format(x2))


def exponent(base: float, e: float):
    return base ** e


def root(base: float, e: float):
    return exponent(base, 1/e)


# -----------------------------------------------------------------------------
# COMPUTE DISCRIMINANT
# -----------------------------------------------------------------------------
def compute_discriminant(a: float, b: float, c: float):
    return exponent(b, 2) - (4 * a * c)



