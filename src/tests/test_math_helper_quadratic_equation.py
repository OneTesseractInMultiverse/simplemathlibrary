from math_helper.quaratic_equation import compute_discriminant, exponent, root


# -----------------------------------------------------------------------------
# TEST
# -----------------------------------------------------------------------------
def test_when_discriminant_is_positive():
    # 1 - Prepare -> Whe prepare the values that will be used in the test case
    a: float = 20
    b: float = 30
    c: float = 10
    expected: float = 100

    # 2 - Compute the actual value
    actual = compute_discriminant(a, b, c)

    # 3 - Assertion
    assert expected == actual
    assert actual > 0


# -----------------------------------------------------------------------------
# TEST
# -----------------------------------------------------------------------------
def test_when_discriminant_is_negative():
    # 1 - Prepare -> Whe prepare the values that will be used in the test case
    a: float = 9
    b: float = 7
    c: float = 12
    expected: float = -383

    # 2 - Compute the actual value
    actual = compute_discriminant(a, b, c)

    # 3 - Assertion
    assert expected == actual
    assert actual < 0


# -----------------------------------------------------------------------------
# TEST
# -----------------------------------------------------------------------------
def test_when_discriminant_is_zero():
    # 1 - Prepare -> Whe prepare the values that will be used in the test case
    a: float = 4
    b: float = -4
    c: float = 1
    expected: float = 0

    # 2 - Compute the actual value
    actual = compute_discriminant(a, b, c)

    # 3 - Assertion
    assert expected == actual
    assert actual == 0


def test_when_base_is_positive_and_exponent_is_positive_result_is_positive():
    base: float = 2
    e: float = 4
    expected: float = 16

    actual = exponent(base, e)

    assert expected == actual
    assert actual > 0


def test_when_base_is_positive_and_exponent_is_negative_result_is_positive_but_less_than_zero():
    base: float = 2
    e: float = -4
    expected: float = 1 / 16

    actual = exponent(base, e)

    assert expected == actual
    assert actual > 0
    assert actual < 1


def test_when_base_is_positive_and_exponent_is_zero_result_is_one():
    base: float = 1250
    e: float = 0
    expected: float = 1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_is_negative_and_exponent_is_zero_result_is_one():
    base: float = -1250
    e: float = 0
    expected: float = 1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_is_negative_and_even_exponent():
    base: float = -1
    e: float = 2
    expected: float = 1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_is_negative_and_odd_exponent():
    base: float = -1
    e: float = 3
    expected: float = -1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_is_positive_and_even_negative_exponent():
    base: float = 1
    e: float = -2
    expected: float = 1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_is_positive_and_odd_negative_exponent():
    base: float = 1
    e: float = -3
    expected: float = 1

    actual = exponent(base, e)

    assert expected == actual


def test_when_base_of_square_root_is_positive():
    base: float = 25
    e: float = 2
    expected: float = 5

    actual = root(base, e)

    assert expected == actual
